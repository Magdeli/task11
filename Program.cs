﻿using System;

namespace Task11
{
    class Program
    {
        static void Main(string[] args)
        {
            // Magdeli Holmøy Asplin
            // 8/19/2019

            // This program contains a payment system where you can use both cash, creditcard
            // and savingscard

            #region Attributes
            // In this region variables and objects are declared

            double totalPrice; //This number represents the total price for each item that can be purchased
            string inputPayment; // This string represents the choice of payment
            string purchasedItem; // This string represents the choice of item to purchase
            Creditcard creditCard = new Creditcard(5000);
            Savingscard savingsCard = new Savingscard(325);
            Cash cash = new Cash(50);
            Person Magdeli = new Person("Magdeli", savingsCard, creditCard, cash) ;

            #endregion

            #region Purchase
            // Here the purchase is made

            Console.WriteLine("What would you like to purchase? We have:");
            Console.WriteLine("Apple - 4.99 kr");
            Console.WriteLine("Washingmachine - 12999 kr");
            Console.WriteLine("Waterbottle - 139 kr");

            purchasedItem = (Console.ReadLine()).ToLower();

            if (purchasedItem == "apple")
            {
                totalPrice = 4.99;
                Console.WriteLine("Would you like to use cash or card?");

                inputPayment = Console.ReadLine().ToLower();

                if (inputPayment == "cash")
                {
                    cash.ReturnChange(totalPrice);
                }

                if (inputPayment == "card")
                {
                    Console.WriteLine("Would you like to pay by creditcard or savingscard?");
                    string cardInput = Console.ReadLine().ToLower();

                    if (cardInput == "creditcard")
                    {
                        creditCard.Withdrawal(totalPrice);
                    }
                    if (cardInput == "savingscard")
                    {
                        savingsCard.Withdrawal(totalPrice);
                    }
                }
            }

            if (purchasedItem == "washingmachine")
            {
                totalPrice = 12999;
                Console.WriteLine("Would you like to use cash or card?");

                inputPayment = Console.ReadLine().ToLower();

                if (inputPayment == "cash")
                {
                    cash.ReturnChange(totalPrice);
                }

                if (inputPayment == "card")
                {
                    Console.WriteLine("Would you like to pay by creditcard or savingscard?");
                    string cardInput = Console.ReadLine().ToLower();

                    if (cardInput == "creditcard")
                    {
                        creditCard.Withdrawal(totalPrice);
                    }
                    if (cardInput == "savingscard")
                    {
                        savingsCard.Withdrawal(totalPrice);
                    }
                }
            }

            if (purchasedItem == "waterbottle")
            {
                totalPrice = 139;
                Console.WriteLine("Would you like to use cash or card?");

                inputPayment = Console.ReadLine().ToLower();

                if (inputPayment == "cash")
                {
                    cash.ReturnChange(totalPrice);
                }

                if (inputPayment == "card")
                {
                    Console.WriteLine("Would you like to pay by creditcard or savingscard?");
                    string cardInput = Console.ReadLine().ToLower();

                    if (cardInput == "creditcard")
                    {
                        creditCard.Withdrawal(totalPrice);
                    }
                    if (cardInput == "savingscard")
                    {
                        savingsCard.Withdrawal(totalPrice);
                    }
                }
            }

            #endregion
        }
    }
}
