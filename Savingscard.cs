﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Savingscard : Card
    {
        // Magdeli Holmøy Asplin
        // 8/19/2019

        // This class can be used to create a savingscard object which contains a balance

        #region Attributes

        #endregion

        #region Constructors

        public Savingscard(double Balance)
        {
            this.Balance = Balance;
        }

        #endregion

        #region Behaviour

        public override void Withdrawal(double totalPrice)
        {
            if (Balance > totalPrice)
            {
                Balance = Balance - totalPrice;

                Console.WriteLine("Payment accepted.");
            }
            else
            {
                Console.WriteLine("You do not have enough money.");
            }

        }

        #endregion
    }
}
