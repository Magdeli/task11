﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Person
    {
        // Magdeli Holmøy Asplin
        // 8/19/2019

        // This class can be used to create a person that has a name, two cards and an amount of cash

        #region Attributes

        private string name;
        private Card card1;
        private Card card2;
        private Cash cash;

        #endregion

        #region Constructors

        public Person(string name, Card card1, Card card2, Cash cash)
        {
            this.name = name;
            this.card1 = card1;
            this.card2 = card2;
            this.cash = cash;
        }

        #endregion

        #region Behaviour

        public string getName()
        {
            return name;
        }



        #endregion
    }
}
