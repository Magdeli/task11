﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Creditcard : Card
    {
        // Magdeli Holmøy Asplin
        // 8/19/2019

        // This class can be used to create a creditcard object which contains a specific credit limit

        #region Attributes

        private double creditLimit;

        #endregion

        #region Constructors

        public Creditcard(double creditLimit)
        {
            this.creditLimit = creditLimit;
        }

        #endregion

        #region Behaviour

        public override void Withdrawal(double totalPrice)
        {
            if(creditLimit > totalPrice)
            {
                creditLimit = creditLimit - totalPrice;
                Console.WriteLine("Payment accepted.");
            }
            else
            {
                Console.WriteLine("Your creditcard is too maxed out for this purchase.");
            }
        }

        #endregion
    }
}
