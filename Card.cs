﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    abstract class Card
    {
        // Magdeli Holmøy Asplin
        // 8/19/2019

        #region Attributes

        private double balance;


        #endregion

        #region Constructors

        public Card() { }

        public double Balance { get => balance; set => balance = value; }

        #endregion

        #region Behaviour
        public abstract void Withdrawal(double totalWithdrawal);
        #endregion
    }
}
