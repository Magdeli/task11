﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task11
{
    class Cash
    {
        // Magdeli Holmøy Asplin
        // 8/19/2019

        // This class can be used to create a cash object which contains a cash amount

        #region Attributes

        double cashAmount;

        #endregion

        #region Constructors

        public Cash(double cashAmount)
        {
            this.cashAmount = cashAmount;
        }

        #endregion

        #region Behaviour

        public void ReturnChange(double totalPrice)
        {
            double change = cashAmount - totalPrice;
            if (change > 0)
            {
                change = Math.Round(change);
                Console.WriteLine($"You get {change} kr back.");
            }
            if (change == 0)
            {
                Console.WriteLine("You paid the exact amount.");
            }
            else
            {
                Console.WriteLine("You do not have enough money.");
            }
            #endregion
        }
    }
}

